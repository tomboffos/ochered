import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Welcome1 from './src/containers/Auth/Welcome1';
import Welcome2 from './src/containers/Auth/Welcome2';
import Login from './src/containers/Auth/Login';
import Register from './src/containers/Auth/Register';
import CatalogDetail from './src/containers/Catalog/Detail';
import CategoryDetail from './src/containers/Category/Detail';
import CompanyDetail from './src/containers/Company/Detail';
import ClientSettings from './src/containers/Client/Settings';
import ClientDetail from './src/containers/Client/Detail';
import Icon from './src/components/Icon';
import {dark, light, primary} from './src/styles/variables';
import {View} from './src/styles';
import CatalogList from './src/containers/Catalog/List';
import AppointmentList from './src/containers/Appointment/List';
import Header from './src/components/Header';
import {Dimensions, Platform} from 'react-native';
import LikeList from './src/containers/Like/List';
import ChatList from './src/containers/Chat/List';
import AppointmentSave from './src/containers/Appointment/Save';
import AppointmentDetail from './src/containers/Appointment/Detail';
import ClientProfile from './src/containers/Client/Profile';
import ClientPassword from './src/containers/Client/Password';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Menu = createMaterialTopTabNavigator();

const OPTIONS = {
  headerShown: false,
  cardStyle: {
    backgroundColor: 'white',
  },
};

export const AuthNavigation = ({onLogin, user, userLoading}) => (
  <NavigationContainer>
    <Stack.Navigator screenOptions={OPTIONS}>
      <Stack.Screen name="Welcome1" component={Welcome1} />
      <Stack.Screen name="Welcome2" component={Welcome2} />
      <Stack.Screen name="Login">
        {props => <Login {...props} user={user} userLoading={userLoading} onLogin={onLogin} />}
      </Stack.Screen>
      <Stack.Screen name="Register">{props => <Register {...props} onLogin={onLogin} />}</Stack.Screen>
    </Stack.Navigator>
  </NavigationContainer>
);

const ProfileStack = ({onLogout}) => (
  <Stack.Navigator screenOptions={OPTIONS}>
    <Stack.Screen name="ClientDetail" component={props => <ClientDetail {...props} onLogout={onLogout} />} />
    <Stack.Screen name="ClientSettings" component={ClientSettings} />
    <Stack.Screen name="ClientProfile" component={ClientProfile} />
    <Stack.Screen name="ClientPassword" component={ClientPassword} />
  </Stack.Navigator>
);

const QueueMenu = () => (
  <Menu.Navigator
    sceneContainerStyle={{backgroundColor: 'white'}}
    style={{paddingTop: Platform.OS === 'ios' && Dimensions.get('window').height > 667 ? 30 : 0}}
  >
    <Menu.Screen name="CatalogList" component={CatalogList} options={{title: 'Встать в очередь'}} />
    <Menu.Screen name="AppointmentList" component={AppointmentList} options={{title: 'Мои очереди'}} />
  </Menu.Navigator>
);

const QueueStack = () => (
  <Stack.Navigator screenOptions={OPTIONS}>
    <Stack.Screen name="AppointmentList" component={AppointmentList} />
    <Stack.Screen name="CatalogList" component={CatalogList} />
    <Stack.Screen name="CatalogDetail" component={CatalogDetail} />
    <Stack.Screen name="CategoryDetail" component={CategoryDetail} />
    <Stack.Screen name="CompanyDetail" component={CompanyDetail} />
    <Stack.Screen name="AppointmentSave" component={AppointmentSave} />
    <Stack.Screen name="AppointmentDetail" component={AppointmentDetail} />
  </Stack.Navigator>
);

const LikeStack = () => (
  <Stack.Navigator screenOptions={OPTIONS}>
    <Stack.Screen name="LikeList" component={LikeList} />
    <Stack.Screen name="CompanyDetail" component={CompanyDetail} />
    <Stack.Screen name="AppointmentSave" component={AppointmentSave} />
    <Stack.Screen name="AppointmentDetail" component={AppointmentDetail} />
  </Stack.Navigator>
);

const ChatStack = () => (
  <Stack.Navigator screenOptions={OPTIONS}>
    <Stack.Screen name="ChatList" component={ChatList} />
  </Stack.Navigator>
);

export const MainNavigation = ({onLogout}) => (
  <NavigationContainer>
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          height: 62,
          paddingBottom: 12,
        },
      }}
    >
      <Tab.Screen
        name="ProfileStack"
        component={props => <ProfileStack {...props} onLogout={onLogout} />}
        options={{
          tabBarIcon: ({focused}) => <Icon name="user-alt" color={focused ? primary : dark} size={26} />,
        }}
      />
      <Tab.Screen
        name="QueueStack"
        component={QueueStack}
        options={{
          tabBarIcon: ({focused}) => <Icon name="clock" color={focused ? primary : dark} size={26} />,
        }}
      />
      <Tab.Screen
        name="LikeStack"
        component={LikeStack}
        options={{
          tabBarIcon: ({focused}) => <Icon name="heart" color={focused ? primary : dark} size={26} />,
        }}
      />
      <Tab.Screen
        name="ChatStack"
        component={ChatStack}
        options={{
          tabBarIcon: ({focused}) => <Icon name="comment-alt" color={focused ? primary : dark} size={26} />,
        }}
      />
    </Tab.Navigator>
  </NavigationContainer>
);
