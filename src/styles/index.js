import React from 'react';
import {
  Text as RNText,
  View as RNView,
  Image as RNImage,
  TextInput as RNTextInput,
  TouchableOpacity as RNTouchableOpacity,
} from 'react-native';

import view from './view';
import text from './text';
import image from './image';

const getItemsByCls = cls => (cls ? cls.split(/(\s+)/).filter(e => e.trim().length > 0) : []);

export const View = ({cls, children, style, reference, ...props}) => (
  <RNView style={[...getItemsByCls(cls).map(i => view[i]), style]} ref={reference} {...props}>
    {children}
  </RNView>
);

export const TouchableOpacity = ({cls, children, style, reference, ...props}) => (
  <RNTouchableOpacity
    style={[{alignItems: 'center', justifyContent: 'center'}, ...getItemsByCls(cls).map(i => view[i] || {}), style]}
    ref={reference}
    {...props}
  >
    {children}
  </RNTouchableOpacity>
);

export const Image = ({cls, children, style, reference, ...props}) => (
  <RNImage
    style={[{width: 16, height: 16}, ...getItemsByCls(cls).map(i => image[i]), style]}
    ref={reference}
    {...props}
  >
    {children}
  </RNImage>
);

export const Text = ({cls, children, style, reference, ...props}) => (
  <RNText style={[{fontSize: 14}, ...getItemsByCls(cls).map(i => text[i]), style]} ref={reference} {...props}>
    {children}
  </RNText>
);

export const TextInput = ({cls, children, style, reference, ...props}) => (
  <RNTextInput style={[{fontSize: 14}, ...getItemsByCls(cls).map(i => text[i]), style]} ref={reference} {...props}>
    {children}
  </RNTextInput>
);
