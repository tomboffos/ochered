export const primary = '#00A6CA'; // '#3D9EFF';
export const primarylight = '#D1D4E8';
export const primarydark = '#3E50B4';

export const secondary = 'royalblue';
export const danger = 'red';
export const success = 'green';
export const warning = 'orange';

export const transparent = 'transparent';
export const white = 'white';
export const light = 'whitesmoke';
export const gray = 'lightgray';
export const dark = 'gray';
export const black = 'black';

export const colors = {
  transparent,
  light,
  gray,
  dark,
  black,
  secondary,
  primary,
  primarylight,
  primarydark,
  white,
  danger,
  success,
  warning,
};
