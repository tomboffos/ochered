import {colors} from './variables';
import layout from './layout';

const getBackgroundColors = () => {
  let result = {};
  for (let i in colors) {
    result[`bg-${i}`] = {backgroundColor: colors[i]};
  }
  return result;
};

const getBorderColors = () => {
  let result = {};
  for (let i in colors) {
    result[`br-${i}`] = {borderColor: colors[i]};
  }
  return result;
};

const view = {
  round: {borderRadius: 8},
  pill: {borderRadius: 20},
  ...getBackgroundColors(),
  ...getBorderColors(),
  ...layout,
};

export default view;
