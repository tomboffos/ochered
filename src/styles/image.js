import view from './view';
import {light} from './variables';

const image = {
  contain: {resizeMode: 'contain'},
  cover: {resizeMode: 'cover'},
  'circle-2': {width: 30, height: 30, borderRadius: 15, resizeMode: 'contain', backgroundColor: light},
  'circle-3': {width: 40, height: 40, borderRadius: 20, resizeMode: 'contain', backgroundColor: light},
  'circle-4': {width: 100, height: 100, borderRadius: 50, resizeMode: 'contain', backgroundColor: light},
  ...view,
};

export default image;
