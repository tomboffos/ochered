import {colors} from './variables';
import view from './view';

const getTextColors = () => {
  let result = {};
  for (let i in colors) {
    result[`t-${i}`] = {color: colors[i]};
  }
  return result;
};

const text = {
  'f-1': {fontSize: 10},
  'f-2': {fontSize: 12},
  'f-3': {fontSize: 14},
  'f-4': {fontSize: 16},
  'f-5': {fontSize: 19},
  'f-6': {fontSize: 26},
  bold: {fontWeight: '900'},
  bolder: {fontWeight: '600'},
  normal: {fontWeight: 'normal'},
  slim: {fontWeight: '100'},
  italic: {fontStyle: 'italic'},
  center: {textAlign: 'center'},
  left: {textAlign: 'left'},
  right: {textAlign: 'right'},
  capitalize: {textTransform: 'capitalize'},
  lowercase: {textTransform: 'lowercase'},
  uppercase: {textTransform: 'uppercase'},
  ...getTextColors(),
  ...view,
};

export default text;
