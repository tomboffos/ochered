import {StyleSheet} from 'react-native';
import {gray, light} from './variables';

const sizes = {
  '1': 2,
  '2': 5,
  '3': 10,
  '4': 15,
  '5': 25,
  auto: 'auto',
};

const spaces = {
  m: 'margin',
  mx: 'marginHorizontal',
  my: 'marginVertical',
  ml: 'marginLeft',
  mr: 'marginRight',
  mt: 'marginTop',
  mb: 'marginBottom',
  p: 'padding',
  px: 'paddingHorizontal',
  py: 'paddingVertical',
  pl: 'paddingLeft',
  pr: 'paddingRight',
  pt: 'paddingTop',
  pb: 'paddingBottom',
};

const getSpacesValues = () => {
  let result = {};
  for (let i in spaces) {
    for (let j in sizes) {
      result[`${i}-${j}`] = {[spaces[i]]: sizes[j]};
    }
  }
  return result;
};

const layout = {
  'ai-start': {alignItems: 'flex-start'},
  'ai-end': {alignItems: 'flex-end'},
  'ai-center': {alignItems: 'center'},
  'ai-stretch': {alignItems: 'stretch'},
  'jc-start': {justifyContent: 'flex-start'},
  'jc-end': {justifyContent: 'flex-end'},
  'jc-center': {justifyContent: 'center'},
  'jc-between': {justifyContent: 'space-between'},
  'jc-evenly': {justifyContent: 'space-evenly'},
  'jc-around': {justifyContent: 'space-around'},
  'br-top': {borderTopWidth: StyleSheet.hairlineWidth, borderColor: '#d7dde2'},
  'br-bottom': {borderBottomWidth: StyleSheet.hairlineWidth, borderColor: '#d7dde2'},
  'br-left': {borderLeftWidth: StyleSheet.hairlineWidth, borderColor: '#d7dde2'},
  'br-right': {borderRightWidth: StyleSheet.hairlineWidth, borderColor: '#d7dde2'},
  'br-bold': {borderWidth: 1},
  br: {borderWidth: 1, borderColor: '#d7dde2'},
  whole: {flex: 1},
  row: {flexDirection: 'row'},
  nowrap: {flexWrap: 'nowrap'},
  hidden: {display: 'none'},
  'w-100': {width: '100%'},
  'w-75': {width: '75%'},
  'w-50': {width: '50%'},
  'w-25': {width: '25%'},
  'h-100': {height: '100%'},
  'h-75': {height: '75%'},
  'h-50': {height: '50%'},
  'h-25': {height: '25%'},
  shadow: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#d7dde2',
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.17,
    shadowRadius: 4,
    elevation: 4,
  },
  ...getSpacesValues(),
};

export default layout;
