import React from 'react';
import {Dimensions, Platform} from 'react-native';
import {useNavigationState, useNavigation} from '@react-navigation/native';
import {View, TouchableOpacity} from '../styles';
import Icon from './Icon';

const Header = ({centerComponent, rightComponent, isEmpty}) => {
  const stateIndex = useNavigationState(v => v.index);
  const navigation = useNavigation();

  return (
    <View style={{paddingTop: Platform.OS === 'ios' && Dimensions.get('window').height > 667 ? 30 : 0}}>
      <View cls={`row ai-center ${!isEmpty ? 'br-bottom' : ''}`} style={{height: 60}}>
        {!!stateIndex && (
          <TouchableOpacity cls="p-3" onPress={() => navigation.goBack()}>
            <Icon name="chevron-left" size={18} color={isEmpty && 'white'} />
          </TouchableOpacity>
        )}
        <View cls={`whole mr-4 ${!stateIndex ? 'ml-4' : ''}`}>{centerComponent}</View>
        {rightComponent}
      </View>
    </View>
  );
};

export default Header;
