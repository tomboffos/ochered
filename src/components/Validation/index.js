import React from 'react';
import {Text} from '../../styles';

const Validation = ({data, field}) => {
  const field_parts = field.split('.');
  const first_part = field_parts[0];
  const last_part = field_parts[1];

  if (data) {
    const method = data ? data[Object.keys(data)[0]] : null;

    if (method.errors) {
      const {errors} = method;
      for (let i in errors) {
        if (errors[i].field === first_part) {
          const messages = errors[i].messages;
          if (!last_part) {
            return <Text cls="mt-2 f-2 t-danger">{messages}</Text>;
          }
          if (messages.includes(last_part)) {
            if (last_part === 'image') {
              return <Text cls="mt-2 f-2 t-danger">Wrong format. Must be jpg or png.</Text>;
            }
            return <Text cls="mt-2 f-2 t-danger">This field may not be blank.</Text>;
          }
        }
      }
    }
  }

  return null;
};

export default Validation;
