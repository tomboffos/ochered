import React from 'react';
import {View, Text} from '../../styles';

export const Loading = () => (
  <View cls="whole p-4 jc-center ai-center">
    <Text>Загрузка...</Text>
  </View>
);

export const Error = ({error}) => {
  console.log('Error', error);
  return (
    <View cls="whole p-4 jc-center ai-center">
      <Text>Ошибка!</Text>
    </View>
  );
};

export const EmptyList = () => {
  return (
    <View cls="whole p-4 jc-center ai-center">
      <Text>Список пуст.</Text>
    </View>
  );
};

const Query = ({loading, error, data}) => {
  const target = data ? data[Object.keys(data)[0]] : null;

  if (loading) {
    return <Loading />;
  }
  if (error) {
    return <Error error={error} />;
  }
  if (Array.isArray(target) && !target.length) {
    return <EmptyList />;
  }
  return null;
};

export default Query;
