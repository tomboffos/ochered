import React, {useRef, useState} from 'react';
import {Animated, Dimensions} from 'react-native';
import {TouchableOpacity, View} from '../../styles';
import Button from '../Button';

const HEIGHT = Dimensions.get('window').height;

const Modal = ({children, toggle}) => {
  const opacity = useRef(new Animated.Value(0)).current;
  const [isOpen, setIsOpen] = useState(false);

  const close = () => {
    Animated.timing(opacity, {
      toValue: 0,
      duration: 400,
      useNativeDriver: true,
    }).start();
    setIsOpen(false);
  };

  const open = () => {
    Animated.timing(opacity, {
      toValue: 0.8,
      duration: 400,
      useNativeDriver: true,
    }).start();
    setIsOpen(true);
  };

  return (
    <>
      {isOpen && (
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            zIndex: 9,
            backgroundColor: 'black',
            opacity,
          }}
        >
          <TouchableOpacity cls="whole" onPress={() => close()} />
        </Animated.View>
      )}
      <Animated.View
        style={{
          position: 'absolute',
          height: HEIGHT - 80,
          top: 0,
          zIndex: 10,
          left: 10,
          right: 10,
          transform: [
            {
              translateY: opacity.interpolate({
                inputRange: [0, 0.8],
                outputRange: [HEIGHT - 110, 80],
              }),
            },
          ],
        }}
      >
        {!isOpen && (
          <TouchableOpacity
            onPress={() => open()}
            style={{position: 'absolute', height: 80, width: '100%', zIndex: 1}}
          />
        )}
        <View
          cls="whole shadow br pt-3 pb-4"
          style={{
            borderTopRightRadius: 15,
            borderTopLeftRadius: 15,
          }}
        >
          <View cls="whole">{children}</View>
          {/*<Button onPress={() => close()} title="Закрыть" type="transparent" />*/}
        </View>
      </Animated.View>
    </>
  );
};

export default Modal;
