import React from 'react';
import {TouchableOpacity, Text} from '../../styles';

const TEXT = {
  primary: 'white',
  transparent: 'dark',
  light: 'black',
  success: 'white',
};

const Button = ({title, onPress, size = 3, type = 'primary', textStyle}) => {
  return (
    <TouchableOpacity cls={`bg-${type} py-${size} px-${size + 1}`} onPress={onPress} style={{borderRadius: size * 7}}>
      <Text cls={`f-${size} t-${TEXT[type]} bold`} style={textStyle}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
