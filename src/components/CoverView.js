import React from 'react';
import {Text, TouchableOpacity, View, Image} from '../styles';
import {useNavigation} from '@react-navigation/native';
import {ScrollView, Dimensions, ImageBackground, Platform, StatusBar} from 'react-native';
import Header from './Header';
import Icon from './Icon';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
const TOP = 100 + (Platform.OS === 'ios' && Dimensions.get('window').height > 667 ? 30 : 0);

const CoverView = ({file, title, isMessage, bottomComponent}) => {
  const navigation = useNavigation();

  return (
    <View cls="whole">
      <StatusBar barStyle="light-content" />
      <ImageBackground
        source={file}
        style={{
          width: WIDTH,
          height: WIDTH * 0.8,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          overflow: 'hidden',
          position: 'absolute',
          top: 0,
        }}
      >
        <View cls="whole" style={{backgroundColor: 'rgba(0,0,0,0.7)'}}>
          <View cls="whole px-4">
            <Header isEmpty centerComponent={title && <Text cls="f-5 bolder">{title}</Text>} />
            <View cls={`row jc-${isMessage ? 'between' : 'center'}`}>
              <Image
                source={require('../images/logo-white.png')}
                style={{width: 120, height: 60, resizeMode: 'contain'}}
              />
              {isMessage && (
                <TouchableOpacity cls="row ai-center" onPress={() => navigation.navigate('ChatStack')}>
                  <Icon name="comment-alt" size={24} />
                  <Text cls="f-4 bold t-white ml-3">Написать</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </ImageBackground>
      <View cls="whole" style={{marginTop: TOP}}>
        <ScrollView>
          <View style={{paddingTop: WIDTH * 0.8 - TOP - 100}}>
            <View style={{minHeight: HEIGHT - WIDTH * 0.8}} cls="py-3 m-3 round shadow">
              {bottomComponent}
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default CoverView;
