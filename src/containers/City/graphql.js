import {gql} from '@apollo/client';

export const QUERY_CITY_LIST = gql`
  query CityList {
    cityList {
      id
      name
    }
  }
`;
