import React from 'react';
import {ActivityIndicator} from 'react-native';
import {TouchableOpacity} from '../../styles';
import {useMutation, useQuery} from '@apollo/client';
import {MUTATION_LIKE_DELETE, MUTATION_LIKE_SAVE, QUERY_LIKE_LIST} from './graphql';
import Icon from '../../components/Icon';
import {updateCreate, updateDelete} from '../../utils/mutation';

const LikeToggle = ({companyId, isBig}) => {
  const likeList = useQuery(QUERY_LIKE_LIST, {fetchPolicy: 'cache-first'});
  const likes = likeList.data ? likeList.data.likeList : [];
  const like = likes.find(x => x.company.id === companyId);

  const [save, saveResp] = useMutation(MUTATION_LIKE_SAVE, {
    update: updateCreate({query: QUERY_LIKE_LIST}),
  });

  const [del, delResp] = useMutation(MUTATION_LIKE_DELETE, {
    update: updateDelete({query: QUERY_LIKE_LIST}),
  });

  return (
    <TouchableOpacity
      disabled={saveResp.loading || delResp.loading}
      onPress={() => {
        like ? del({variables: {input: {id: like.id}}}) : save({variables: {input: {company: companyId}}});
      }}
    >
      {saveResp.loading || delResp.loading ? (
        <ActivityIndicator size={isBig ? 40 : 24} />
      ) : (
        <Icon name="heart-solid" size={isBig ? 40 : 24} color={like ? 'red' : 'gray'} />
      )}
    </TouchableOpacity>
  );
};

export default LikeToggle;
