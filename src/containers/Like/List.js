import React from 'react';
import {ScrollView} from 'react-native';
import {View, Text} from '../../styles';
import {useQuery} from '@apollo/client';
import {QUERY_LIKE_LIST} from './graphql';
import Header from '../../components/Header';
import Query from '../../components/Query';
import CompanyItem from '../Company/Item';

const LikeList = () => {
  const query = useQuery(QUERY_LIKE_LIST, {fetchPolicy: 'cache-first'});

  return (
    <View cls="whole bg-white">
      <Header centerComponent={<Text cls="bolder f-5">Избранное</Text>} />
      <ScrollView>
        <View cls="py-4">
          {Query(query) || query.data.likeList.map(item => <CompanyItem key={item.id} item={item.company} withHeart />)}
        </View>
      </ScrollView>
    </View>
  );
};

export default LikeList;
