import {gql} from '@apollo/client';
import {FRAGMENT_COMPANY_LIST} from '../Company/graphql';

export const QUERY_LIKE_LIST = gql`
  query LikeList {
    likeList {
      id
      company {
        ...CompanyList
      }
    }
  }
  ${FRAGMENT_COMPANY_LIST}
`;

export const MUTATION_LIKE_SAVE = gql`
  mutation LikeSave($input: LikeSaveInput!) {
    likeSave(input: $input) {
      obj {
        id
        company {
          ...CompanyList
        }
      }
    }
  }
  ${FRAGMENT_COMPANY_LIST}
`;

export const MUTATION_LIKE_DELETE = gql`
  mutation LikeDelete($input: LikeDeleteInput!) {
    likeDelete(input: $input) {
      obj {
        id
      }
    }
  }
`;
