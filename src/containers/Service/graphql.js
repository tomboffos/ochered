import {gql} from '@apollo/client';

export const QUERY_SERVICE_LIST = gql`
  query ServiceList($companyId: ID) {
    serviceList(group_Company: $companyId) {
      id
      name
      duration
    }
  }
`;
