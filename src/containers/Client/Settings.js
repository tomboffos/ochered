import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from '../../styles';
import {primary} from '../../styles/variables';
import CheckBox from '@react-native-community/checkbox';
import Icon from '../../components/Icon';
import Header from '../../components/Header';
import {ScrollView} from 'react-native';
import RNModal from 'react-native-modal';
import {useMutation, useQuery} from '@apollo/client';
import {QUERY_CITY_LIST} from '../City/graphql';
import {MUTATION_CLIENT_SAVE, QUERY_USER} from './graphql';

const ClientSettings = ({navigation}) => {
  const {user} = useQuery(QUERY_USER, {fetchPolicy: 'cache-only'}).data;
  const {city} = user.client;

  const [isPush, setIsPush] = useState(true);
  const [select, setSelect] = useState(null);

  const cityList = useQuery(QUERY_CITY_LIST);
  const cities = cityList.data ? cityList.data.cityList : [];

  const [save, saveResp] = useMutation(MUTATION_CLIENT_SAVE);

  const input = {
    id: user.client.id,
    city: user.client.city.id,
    user: {
      avatar: user.avatar,
      username: user.username,
      name: user.name,
      password: '',
    },
  };

  const onSave = updates => {
    save({variables: {input: {...input, ...updates}}});
  };

  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="f-5 bolder">Настройки</Text>} />
      <View cls="whole">
        <View cls="row jc-between ai-center p-4 br-bottom">
          <Text cls="bolder">Push уведомления</Text>
          <CheckBox
            value={isPush}
            onValueChange={newValue => setIsPush(newValue)}
            tintColors={{
              true: primary,
            }}
            onTintColor={primary}
            onCheckColor={primary}
            animationDuration={0.2}
          />
        </View>
        <TouchableOpacity onPress={() => setSelect('city')} cls="ai-start p-4 br-bottom">
          <View>
            <Text cls="t-dark f-2 mb-2">Город</Text>
            <Text cls="bolder">{city.name}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('ClientProfile', {input})}
          cls="row jc-between px-4 py-5 br-bottom"
        >
          <Text cls="bolder">Редактировать профиль</Text>
          <Icon name="chevron-right" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ClientPassword', {input})} cls="row jc-between px-4 py-5 br-bottom">
          <Text cls="bolder">Изменить пароль</Text>
          <Icon name="chevron-right" />
        </TouchableOpacity>
      </View>
      <RNModal isVisible={select === 'city'} useNativeDriver={true} onBackdropPress={() => setSelect(null)}>
        <View
          cls="whole bg-white p-4"
          style={{marginTop: 200, marginBottom: -20, borderTopLeftRadius: 15, borderTopRightRadius: 15}}
        >
          <Text cls="bold py-3">Выбрать категорию</Text>
          <ScrollView>
            <View cls="py-4">
              {cities.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() => {
                    onSave({city: item.id});
                    setSelect(null);
                  }}
                  cls={`row jc-between br round mb-3 px-3 py-4 ${city.id === item.id && 'bg-primary'}`}
                >
                  <Text cls={`bold left ${city.id === item.id && 't-white'}`}>{item.name}</Text>
                  {item.id === city.id && <Icon name="check" color="white" />}
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </View>
      </RNModal>
    </View>
  );
};

export default ClientSettings;
