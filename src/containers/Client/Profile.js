import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {View, Text, TextInput, Image, TouchableOpacity} from '../../styles';
import Header from '../../components/Header';
import Button from '../../components/Button';
import {useMutation, useQuery} from '@apollo/client';
import {MUTATION_CLIENT_SAVE, QUERY_USER} from './graphql';
import Validation from '../../components/Validation';
// import DocumentPicker from 'react-native-document-picker';

const ClientProfile = ({route, navigation}) => {
  const {input} = route.params;

  const {user} = useQuery(QUERY_USER, {fetchPolicy: 'cache-only'}).data;

  const [name, setName] = useState(user.name);
  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);

  const [save, saveResp] = useMutation(MUTATION_CLIENT_SAVE, {
    onCompleted: data => {
      if (data.clientSave.obj) {
        navigation.goBack();
      }
    },
  });

  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="f-5 bolder">Редактировать профиль</Text>} />
      <ScrollView>
        <View cls="p-4">
          <View>
            <Text cls="t-dark">Аватар</Text>
            <View cls="ai-center">
              <View cls="bg-light mt-3 " style={{width: 170, height: 170, borderRadius: 85}}>
                {user.avatar && <Image source={{uri: user.avatar.path}} style={{width: '100%', height: '100%'}} />}
              </View>
              <TouchableOpacity
                cls="bg-dark round p-3 mt-3"
                onPress={() => {
                  // DocumentPicker.pick({
                  //   type: [DocumentPicker.types.images],
                  // }).then(res => {
                  //   console.log(
                  //     res.uri,
                  //     res.type, // mime type
                  //     res.name,
                  //     res.size
                  //   );
                  // });
                }}
              >
                <Text cls="t-white">Выбрать файл</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View cls="mt-4">
            <Text cls="t-dark">Ваше полное имя</Text>
            <TextInput value={name} onChangeText={v => setName(v)} cls="p-4 bg-light round mt-3 f-4" />
          </View>
          <View cls="mt-4">
            <Text cls="t-dark">Email</Text>
            <TextInput autoCapitalize={false} value={email} onChangeText={v => setEmail(v)} cls="p-4 bg-light round mt-3 f-4" />
          </View>
          <View cls="mt-4">
            <Text cls="t-dark">Номер телефона</Text>
            <TextInput value={username} onChangeText={v => setUsername(v)} cls="p-4 bg-light round mt-3 f-4" />
          </View>
          <Validation data={saveResp.data} field="user.username" />
          <View cls="mt-5">
            <Button
              title="Сохранить"
              textStyle={{padding: 5}}
              onPress={() =>
                save({
                  variables: {
                    input: {
                      ...input,
                      user: {
                        ...input.user,
                        name,
                        email,
                        username,
                      },
                    },
                  },
                })
              }
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ClientProfile;
