import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {Text, TextInput, View} from '../../styles';
import Header from '../../components/Header';
import Button from '../../components/Button';
import {useMutation} from '@apollo/client';
import {MUTATION_CLIENT_SAVE} from './graphql';

const ClientPassword = ({route, navigation}) => {
  const {input} = route.params;

  const [oldPassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');

  const [save, saveResp] = useMutation(MUTATION_CLIENT_SAVE, {
    onCompleted: data => {
      if (data.clientSave.obj) {
        navigation.goBack();
      }
    },
  });
  console.log(saveResp);

  const onSave = () => {
    if (!password || !oldPassword) {
      return false;
    }

    save({
      variables: {
        input: {
          ...input,
          user: {
            ...input.user,
            oldPassword,
            password,
          },
        },
      },
    });
  };

  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="f-5 bolder">Изменить пароль</Text>} />
      <ScrollView>
        <View cls="p-4">
          <View cls="mt-4">
            <Text cls="t-dark">Ваш текущий пароль</Text>
            <TextInput secureTextEntry={true} value={oldPassword} onChangeText={v => setOldPassword(v)} cls="p-4 bg-light round mt-3 f-4" />
            {saveResp.data && saveResp.data.clientSave && saveResp.data.clientSave.errors && <Text cls="f-2 t-danger mt-2">Invalid password.</Text>}
          </View>
          <View cls="mt-4">
            <Text cls="t-dark">Новый пароль</Text>
            <TextInput secureTextEntry={true} value={password} onChangeText={v => setPassword(v)} cls="p-4 bg-light round mt-3 f-4" />
          </View>
          <View cls="mt-5">
            <Button
              title="Сохранить"
              textStyle={{padding: 5}}
              onPress={onSave}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ClientPassword;
