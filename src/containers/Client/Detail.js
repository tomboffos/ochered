import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Image} from '../../styles';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import Modal from '../../components/Modal';
import CatalogList from '../Catalog/List';
import QueueList from '../Appointment/List';
import {useQuery} from '@apollo/client';
import {QUERY_USER} from './graphql';
import Header from '../../components/Header';

const ClientDetail = ({navigation, onLogout}) => {
  const {user} = useQuery(QUERY_USER, {fetchPolicy: 'cache-only'}).data;

  const [page, setPage] = useState('CatalogList');

  return (
    <View cls="whole">
      <Header
        centerComponent={<Text cls="f-5 bolder">Профиль</Text>}
        rightComponent={
          <TouchableOpacity cls="p-4" onPress={() => navigation.push('ClientSettings')}>
            <Icon name="cog" size={22} />
          </TouchableOpacity>
        }
      />
      <View cls="whole jc-center ai-center">
        <View cls="bg-light" style={{width: 170, height: 170, borderRadius: 85}}>
          {user.avatar && <Image source={{uri: user.avatar.path}} style={{width: '100%', height: '100%'}} />}
        </View>
        <Text cls={`f-6 bolder px-5 center my-4 ${!user.name ? 't-dark' : ''}`}>{user.name || 'Без имени'}</Text>
        <TouchableOpacity cls="mt-5 bg-light py-3 round px-5" onPress={onLogout}>
          <Text>Выйти</Text>
        </TouchableOpacity>
      </View>

      {/*<Modal>*/}
      {/*  <View cls="bg-primary mb-3" style={{width: 120, height: 7, borderRadius: 10, alignSelf: 'center'}} />*/}
      {/*  <View cls="row jc-around">*/}
      {/*    <Button*/}
      {/*      onPress={() => {*/}
      {/*        setPage('CatalogList');*/}
      {/*      }}*/}
      {/*      title="Встать в очередь"*/}
      {/*      type="transparent"*/}
      {/*      textStyle={{color: page !== 'CatalogList' ? 'gray' : 'black'}}*/}
      {/*    />*/}
      {/*    <Button*/}
      {/*      onPress={() => {*/}
      {/*        setPage('QueueList');*/}
      {/*      }}*/}
      {/*      title="Мой очереди"*/}
      {/*      type="transparent"*/}
      {/*      textStyle={{color: page !== 'QueueList' ? 'gray' : 'black'}}*/}
      {/*    />*/}
      {/*  </View>*/}
      {/*  {page === 'CatalogList' && <CatalogList />}*/}
      {/*  {page === 'QueueList' && <QueueList client={user.client} />}*/}
      {/*</Modal>*/}
    </View>
  );
};

export default ClientDetail;
