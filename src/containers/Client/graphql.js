import {gql} from '@apollo/client';

export const FRAGMENT_CLIENT = gql`
  fragment Client on ClientType {
    id
    city {
      id
      name
    }
  }
`;

export const FRAGMENT_USER = gql`
  fragment User on UserType {
    id
    username
    name
    phone
    email
    role
    avatar {
      id
      path
    }
  }
`;

export const QUERY_USER = gql`
  {
    user {
      ...User
      client {
        ...Client
      }
    }
  }
  ${FRAGMENT_CLIENT}
  ${FRAGMENT_USER}
`;

export const MUTATION_CLIENT_SAVE = gql`
  mutation ClientSave($input: ClientSaveInput!) {
    clientSave(input: $input) {
      obj {
        ...Client
        user {
          ...User
        }
      }
      errors {
        field
        messages
      }
    }
  }
  ${FRAGMENT_CLIENT}
  ${FRAGMENT_USER}
`;
