import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Image, Text, TouchableOpacity, View} from '../../styles';
import Icon from '../../components/Icon';
import LikeToggle from '../Like/Toggle';

const CompanyItem = ({item, withHeart}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => navigation.push('CompanyDetail', {company: item})}
      cls="row ai-center shadow round mx-3 mb-3 px-3 py-4"
    >
      <Image cls="circle-3" source={item.logo ? {uri: item.logo.path} : require('../../images/point.png')} />
      <View cls="whole mx-3">
        <Text cls="bolder mb-2">{item.name}</Text>
        <View cls="row">
          {[1, 2, 3, 4, 5].map(i => (
            <View key={i} cls="mr-3">
              <Icon name={i <= item.scoresCount / item.appointmentsCount ? 'star' : 'star-solid'} color="orange" />
            </View>
          ))}
        </View>
      </View>
      {withHeart ? <LikeToggle companyId={item.id} /> : <Icon name="chevron-right" size={20} />}
    </TouchableOpacity>
  );
};

export default CompanyItem;
