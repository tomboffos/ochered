import React, {useState} from 'react';
import {useQuery} from '@apollo/client';
import {QUERY_COMPANY_LIST} from './graphql';
import Query from '../../components/Query';
import CompanyItem from './Item';
import {QUERY_USER} from '../Client/graphql';
import {TextInput, View} from '../../styles';
import Icon from '../../components/Icon';

const CompanyList = ({category}) => {
  const [search, setSearch] = useState('');

  const {user} = useQuery(QUERY_USER, {fetchPolicy: 'cache-only'}).data;

  const query = useQuery(QUERY_COMPANY_LIST, {
    variables: {categoryId: category.id, cityId: user.client.city.id, search},
  });

  return (
    <View>
      <View cls="row ai-center round mx-3 mb-4 bg-light">
        <View cls="px-4">
          <Icon name="search" />
        </View>
        <TextInput value={search} onChangeText={v => setSearch(v)} cls="whole py-4" placeholder="Поиск" />
      </View>
      {Query(query) || query.data.companyList.map(item => <CompanyItem key={item.id} item={item} />)}
    </View>
  );
};

export default CompanyList;
