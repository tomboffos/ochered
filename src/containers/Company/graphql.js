import {gql} from '@apollo/client';

export const FRAGMENT_COMPANY_LIST = gql`
  fragment CompanyList on CompanyType {
    id
    name
    appointmentsCount
    scoresCount
    start
    end
    logo {
      id
      path
    }
    cover {
      id
      path
    }
  }
`;

export const QUERY_COMPANY_LIST = gql`
  query CompanyList($categoryId: ID!, $cityId: ID, $search: String) {
    companyList(category: $categoryId, city: $cityId, name_Icontains: $search) {
      ...CompanyList
    }
  }
  ${FRAGMENT_COMPANY_LIST}
`;
