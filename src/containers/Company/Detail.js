import React from 'react';
import {View, Text, Image, TouchableOpacity} from '../../styles';
import Header from '../../components/Header';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import LikeToggle from '../Like/Toggle';
import CoverView from '../../components/CoverView';

const PointDetail = ({route, navigation}) => {
  const {company} = route.params;

  return (
    <View cls="whole">
      <CoverView
        file={company.cover ? {uri: company.cover.path} : require('../../images/point-cover.png')}
        isMessage
        bottomComponent={
          <View cls="whole ai-center jc-between py-4">
            <View>
              <View cls="ai-center">
                <Image cls="circle-4" source={{uri: company.logo && company.logo.path}} />
              </View>
              <Text cls="f-5 bolder my-3 center">{company.name}</Text>
              <View cls="row">
                {[1, 2, 3, 4, 5].map(item => (
                  <View key={item} cls="mx-2">
                    <Icon
                      name={item <= company.scoresCount / company.appointmentsCount ? 'star' : 'star-solid'}
                      color="orange"
                      size={20}
                    />
                  </View>
                ))}
              </View>
            </View>
            <LikeToggle companyId={company.id} isBig />
            <View>
              <Text cls="center t-dark">Встать в очередь</Text>
              <View cls="my-3">
                <Button title="Живая очередь" onPress={() => navigation.navigate('AppointmentSave', {company})} />
              </View>
              <View>
                <Button
                  title="Очередь по записи"
                  onPress={() => navigation.navigate('AppointmentSave', {company, isTime: true})}
                />
              </View>
            </View>
          </View>
        }
      />
    </View>
  );
};

export default PointDetail;
