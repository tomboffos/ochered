import React from 'react';
import {Dimensions} from 'react-native';
import {View, Text, Image} from '../../styles';
import Button from '../../components/Button';
import Icon from '../../components/Icon';

const WIDTH = Dimensions.get('window').width;

const Welcome1 = ({navigation}) => {
  return (
    <View cls="whole jc-between py-5">
      <View cls="mt-5 px-5 ai-center">
        <Image source={require('../../images/logo.png')} style={{width: 200, height: 60, resizeMode: 'contain'}} />
      </View>
      <View>
        <Text cls="bold px-5" style={{fontSize: 36}}>
          Будь всегда первым!
        </Text>
        <Image style={{width: WIDTH, height: 280 * (WIDTH / 360)}} source={require('../../images/welcome-1.png')} />
        <Text cls="center mt-5 px-5">Больше никаких очередй и ожиданий просто будь первым всегда</Text>
      </View>
      <View cls="row jc-between ai-center px-5">
        <Button title="Пропустить" type="transparent" onPress={() => navigation.push('Login')} />
        <Button title="Далее" type="light" onPress={() => navigation.push('Welcome2')} />
      </View>
    </View>
  );
};

export default Welcome1;
