import React, {useState} from 'react';
import {Dimensions} from 'react-native';
import {View, Text, Image, TextInput} from '../../styles';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import {request} from '../../utils/api';

const WIDTH = Dimensions.get('window').width;

const Login = ({navigation, onLogin, user, userLoading}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState('');
  const [error, setError] = useState(null);

  const login = () => {
    setLoading(true);
    setError(null);
    request({method: 'post', url: '/api-token-auth', data: {username, password}})
      .then(res => {
        console.log(res);
        if (res.data.token) {
          onLogin(res.data.token);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
        setError(e.response.data);
        setLoading(false);
      });
  };

  return (
    <View cls="whole jc-between py-5">
      <View cls="mt-5 px-5 ai-center">
        <Image source={require('../../images/logo.png')} style={{width: 200, height: 60, resizeMode: 'contain'}} />
      </View>
      <View>
        <View cls="ai-center mb-4">
          <TextInput
            value={username}
            onChangeText={v => setUsername(v)}
            cls="br p-4 center"
            style={{width: 250, borderRadius: 30}}
            placeholder="Номер телефона"
          />
          {error && error.username && <Text cls="mt-2 f-2 t-danger">{error.username}</Text>}
          <TextInput
            value={password}
            secureTextEntry={true}
            onChangeText={v => setPassword(v)}
            cls="br p-4 mt-4 center"
            style={{width: 250, borderRadius: 30}}
            placeholder="Пароль"
          />
          {error && error.password && <Text cls="mt-2 f-2 t-danger">{error.password}</Text>}
          {error && error.non_field_errors && <Text cls="t-danger center mt-3">{error && error.non_field_errors}</Text>}
          {!loading && !error && user && user.role !== 'CLIENT' ? (
            <Text cls="t-danger center mt-3">Доступ запрещен</Text>
          ) : null}
        </View>
        <Image style={{width: WIDTH, height: 280 * (WIDTH / 360)}} source={require('../../images/welcome-3.png')} />
      </View>
      <View cls="row jc-between ai-center px-5">
        <Button title="Регистрация" type="light" onPress={() => navigation.push('Register')} />
        <Button title={loading || userLoading ? 'Отправка' : 'Войти'} onPress={login} />
      </View>
    </View>
  );
};

export default Login;
