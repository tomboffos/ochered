import React, {useState} from 'react';
import CheckBox from '@react-native-community/checkbox';
import {Image, Text, TextInput, View} from '../../styles';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import {primary} from '../../styles/variables';
import {useMutation} from '@apollo/client';
import {MUTATION_CLIENT_SAVE} from '../Client/graphql';
import Validation from '../../components/Validation';
import {request} from '../../utils/api';

const Register = ({onLogin, navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);

  const [save, {loading, data}] = useMutation(MUTATION_CLIENT_SAVE, {
    update: (cache, {data: {clientSave}}) => {
      if (clientSave.obj) {
        setIsRegistered(true);
        request({
          method: 'post',
          url: '/api-token-auth',
          data: {username, password},
        }).then(res => {
          if (res.data.token) {
            onLogin(res.data.token);
          }
        });
      }
    },
  });

  if (isRegistered) {
    return (
      <View cls="whole ai-center jc-center">
        <Text cls="text-primary">Загрузка...</Text>
      </View>
    );
  }

  return (
    <View cls="whole jc-between py-5">
      <View cls="mt-5 px-5 ai-center">
        <Image source={require('../../images/logo.png')} style={{width: 200, height: 60, resizeMode: 'contain'}} />
      </View>
      <View>
        <Text cls="bold center" style={{fontSize: 36}}>
          Регистрация
        </Text>
        <View cls="ai-center my-4">
          <TextInput
            value={username}
            onChangeText={v => setUsername(v)}
            cls="br p-4 center"
            style={{width: 250, borderRadius: 30}}
            placeholder="Номер телефона"
          />
          <Validation data={data} field="user.username" />
          <TextInput
            value={password}
            secureTextEntry={true}
            onChangeText={v => setPassword(v)}
            cls="br p-4 mt-4 center"
            style={{width: 250, borderRadius: 30}}
            placeholder="Пароль"
          />
          <Validation data={data} field="user.password" />
        </View>
        <View cls="row jc-center ai-center">
          <CheckBox
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            tintColors={{
              true: primary,
            }}
            onTintColor={primary}
            onCheckColor={primary}
            animationDuration={0.2}
          />
          <Text cls="ml-3">Согласен с правилами пользования</Text>
        </View>
      </View>
      <View cls="row jc-between ai-center px-5">
        <Button title="Назад" type="transparent" onPress={() => navigation.goBack()} />
        <Button
          title={loading ? 'Отправка ...' : 'Зарегестрируйся'}
          onPress={() => {
            if (toggleCheckBox) {
              save({
                variables: {
                  input: {
                    user: {
                      username,
                      password,
                    },
                  },
                },
              });
            }
          }}
        />
      </View>
    </View>
  );
};

export default Register;
