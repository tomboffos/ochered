export const STATUSES = {
  WAITING: 'В очереди',
  PROCESSING: 'В процессе',
  FINISHED: 'Закрыта',
};
