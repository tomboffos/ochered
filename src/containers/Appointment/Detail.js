import React, {useState} from 'react';
import {useMutation, useQuery} from '@apollo/client';
import {
  MUTATION_APPOINTMENT_DELETE,
  MUTATION_APPOINTMENT_SCORE,
  QUERY_APPOINTMENT_DETAIL,
  QUERY_APPOINTMENT_LIST,
} from './graphql';
import {View, Text, TouchableOpacity, TextInput, Image} from '../../styles';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import moment from 'moment';
import {STATUSES} from './constants';
import CoverView from '../../components/CoverView';
import {updateDelete} from '../../utils/mutation';

const AppointmentDetail = ({route, navigation}) => {
  const query = useQuery(QUERY_APPOINTMENT_DETAIL, {variables: {id: route.params.data.id}});
  const data = query.data ? query.data.appointmentDetail : route.params.data;

  const [score, setScore] = useState(data.score);
  const [commentClient, setCommentClient] = useState(data.commentClient);

  const [make_score, resp] = useMutation(MUTATION_APPOINTMENT_SCORE);
  const [del, delResp] = useMutation(MUTATION_APPOINTMENT_DELETE, {
    update: updateDelete({query: QUERY_APPOINTMENT_LIST}, () => navigation.goBack()),
  });

  if (!data) {
    return null;
  }

  const {company} = data.service.group;

  return (
    <View cls="whole">
      <CoverView
        file={company.cover ? {uri: company.cover.path} : require('../../images/point-cover.png')}
        title={<Text cls="bolder f-5 t-white">{data.number ? 'Живая очередь' : 'Очеред по записи'}</Text>}
        isMessage
        bottomComponent={
          <View cls="whole ai-center">
            <Image cls="circle-4" source={{uri: company.logo.path}} />
            <Text cls="f-5 my-3 center">{company.name}</Text>
            <Text cls="bolder my-3 mb-4 center" style={{fontSize: 32}}>
              {data.number ? `№ ${data.number}` : moment(data.start).format('DD MMM | HH:mm')}
            </Text>
            <View cls="px-4 py-2 bg-primary pill">
              <Text cls="t-white bolder f-2 center">{STATUSES[data.status]}</Text>
            </View>
            <Text cls="f-2 t-dark center mt-3">{moment(data.createdAt).format('DD MMM YYYY, HH:mm')}</Text>
            {data.master && (
              <View cls="bg-light mx-4 p-4 mt-5" style={{alignSelf: 'stretch'}}>
                <View cls="row ai-center round bg-light">
                  <Image
                    source={{uri: data.master.user.avatar && data.master.user.avatar.path}}
                    cls="circle-3"
                    style={{backgroundColor: 'whit'}}
                  />
                  <View cls="ml-3">
                    <Text cls="bolder">{data.master.user.name}</Text>
                    <Text cls="mt-1 f-2">{data.master.specialty}</Text>
                  </View>
                </View>
                {data.status === 'FINISHED' && (
                  <View>
                    <View cls="row mt-3">
                      {[1, 2, 3, 4, 5].map(r => (
                        <TouchableOpacity key={r} onPress={() => !data.score && setScore(r)} cls="mr-4">
                          <Icon name="star-solid" size={36} color={r <= score ? 'orange' : 'lightgray'} />
                        </TouchableOpacity>
                      ))}
                    </View>
                    {data.score ? (
                      data.commentClient ? (
                        <Text cls="mt-3">{data.commentClient}</Text>
                      ) : null
                    ) : (
                      <TextInput
                        style={{minHeight: 50}}
                        multiline={true}
                        disabled={!!data.score}
                        value={commentClient}
                        onChangeText={v => setCommentClient(v)}
                        cls="round p-3 bg-white mt-3"
                        placeholder="Можете оставить комментарий"
                      />
                    )}
                    {!data.score &&
                      (score ? (
                        <View cls="mt-4">
                          <Button
                            title={resp.loading ? 'Отправка...' : 'Оценить'}
                            type="warning"
                            onPress={() =>
                              make_score({
                                variables: {
                                  input: {
                                    id: data.id,
                                    score,
                                    commentClient: commentClient || '',
                                  },
                                },
                              })
                            }
                          />
                        </View>
                      ) : (
                        <Text cls="f-2 t-dark center mt-4">Оцените</Text>
                      ))}
                  </View>
                )}
              </View>
            )}
            {data.status === 'WAITING' && (
              <TouchableOpacity
                disabled={delResp.loading}
                onPress={() => del({variables: {input: {id: data.id}}})}
                cls="mx-4 p-4 bg-light mt-5"
                style={{alignSelf: 'stretch'}}
              >
                <Text>{delResp.loading ? 'Отмена ...' : 'Отменить очередь'}</Text>
              </TouchableOpacity>
            )}
          </View>
        }
      />
    </View>
  );
};

export default AppointmentDetail;
