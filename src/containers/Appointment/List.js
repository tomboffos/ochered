import React from 'react';
import {TouchableOpacity, View} from '../../styles';
import {FlatList} from 'react-native';
import AppointmentListItem from './ListItem';
import {useQuery} from '@apollo/client';
import {QUERY_APPOINTMENT_LIST} from './graphql';
import Query from '../../components/Query';
import Header from '../../components/Header';
import Icon from '../../components/Icon';
import {Text} from '../../styles';

const AppointmentList = ({navigation}) => {
  const appointmentList = useQuery(QUERY_APPOINTMENT_LIST, {fetchPolicy: 'cache-and-network', pollInterval: 2000});

  return (
    <View cls="whole">
      <Header
        centerComponent={<Text cls="bolder f-5">Мои очереди</Text>}
        rightComponent={
          <TouchableOpacity cls="p-4" onPress={() => navigation.navigate('CatalogList')}>
            <Icon name="plus" size={24} />
          </TouchableOpacity>
        }
      />
      {Query(appointmentList) || (
        <View cls="whole py-4">
          <FlatList
            data={appointmentList.data.appointmentList}
            style={{paddingVertical: 10}}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <TouchableOpacity cls="ai-stretch" onPress={() => navigation.navigate('AppointmentDetail', {data: item})}>
                <AppointmentListItem item={item} />
              </TouchableOpacity>
            )}
          />
        </View>
      )}
    </View>
  );
};

export default AppointmentList;
