import React from 'react';
import {Image, Text, TouchableOpacity, View} from '../../styles';
import moment from 'moment';
import {STATUSES} from './constants';
import Icon from '../../components/Icon';

const AppointmentListItem = ({item, isDetail}) => {
  return (
    <View cls="shadow ai-stretch round mx-3 mb-4">
      <View cls="p-4">
        <View cls="row ai-center">
          <Image
            source={
              item.service.group.company.logo ? {uri: item.service.group.company.logo.path} : require('../../images/point.png')
            }
            style={{width: 30, height: 30, borderRadius: 15, resizeMode: 'cover'}}
            cls="bg-light"
          />
          <Text cls="ml-3">{item.service.group.company.name}</Text>
          <View cls="px-4 py-2 bg-primary pill ml-auto">
            <Text cls="t-white bolder f-2 center">{STATUSES[item.status]}</Text>
          </View>
        </View>
        <Text cls="bolder my-3 center" style={{fontSize: 26}}>
          {item.number ? `№ ${item.number}` : moment(item.start).format('DD MMM | HH:mm')}
        </Text>
        <Text cls="f-2 t-dark center">
          {item.number ? 'Живая очередь' : 'Очеред по записи'}, {moment(item.createdAt).format('DD MMM YYYY, HH:mm')}
        </Text>
      </View>
      <View cls={`row jc-${item.master ? 'between' : 'center'} ${item.status === 'PROCESSING' && 'bg-primary'} ai-center br-top br-gray p-3`}>
        {item.master && (
          <View cls="row ai-center">
            <Image source={{uri: item.master.user.avatar && item.master.user.avatar.path}} cls="circle-3" />
            <View cls="ml-3">
              <Text cls={`bolder ${item.status === 'PROCESSING' && 't-white'}`}>{item.master.user.name}</Text>
              <Text cls={`mt-1 f-2 ${item.status === 'PROCESSING' && 't-white'}`}>{item.master.specialty}</Text>
            </View>
          </View>
        )}
        {item.status === 'WAITING' && <Text cls="center">Отменить {!item.master && 'очередь'}</Text>}
        {item.status === 'FINISHED' && (
          <View cls="row jc-center">
            {[1, 2, 3, 4, 5].map(r => (
              <View key={r} cls="mx-2">
                <Icon name="star-solid" size={18} color={r <= item.score ? 'orange' : 'lightgray'} />
              </View>
            ))}
          </View>
        )}
      </View>
    </View>
  );
};

export default AppointmentListItem;
