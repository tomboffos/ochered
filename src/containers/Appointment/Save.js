import React, {useState} from 'react';
import {Dimensions, ScrollView} from 'react-native';
import {TouchableOpacity, View, Text, Image} from '../../styles';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import RNModal from 'react-native-modal';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import {primary} from '../../styles/variables';
import {useMutation, useQuery} from '@apollo/client';
import {MUTATION_APPOINTMENT_SAVE, QUERY_APPOINTMENT_LIST} from './graphql';
import {QUERY_SERVICE_LIST} from '../Service/graphql';
import CoverView from '../../components/CoverView';
import Header from '../../components/Header';
import {updateCreate} from '../../utils/mutation';
import {QUERY_POSITION_LIST} from '../Position/graphql';

const WIDTH = Dimensions.get('window').width;

const AppointmentSave = ({route, navigation}) => {
  const {company, isTime} = route.params;

  const [select, setSelect] = useState(null);
  const [date, setDate] = useState(null);
  const [time, setTime] = useState(null);
  const [service, setService] = useState(null);
  const [master, setMaster] = useState(null);

  const appointmentList = useQuery(QUERY_APPOINTMENT_LIST, {
    skip: !date,
    variables: {
      companyId: company.id,
      start: moment(date).startOf('day').format(),
      end: moment(date).endOf('day').format(),
    },
  });
  const appointments = appointmentList.data ? appointmentList.data.appointmentList : [];

  const positionList = useQuery(QUERY_POSITION_LIST, {variables: {serviceId: service && service.id}, skip: !service});
  const positions = positionList.data ? positionList.data.positionList : [];

  const serviceList = useQuery(QUERY_SERVICE_LIST, {variables: {companyId: company.id}});
  const services = serviceList.data ? serviceList.data.serviceList : [];

  const [save, saveResp] = useMutation(MUTATION_APPOINTMENT_SAVE, {
    update: updateCreate({query: QUERY_APPOINTMENT_LIST}, obj => navigation.replace('AppointmentDetail', {data: obj})),
  });

  const onSave = () => {
    if (service && (!isTime || (date && time))) {
      save({
        variables: {
          input: {
            service: service.id,
            master: master && master.id,
            start:
              isTime &&
              moment(date)
                .hour(parseInt(time / 60))
                .minute(time % 60)
                .format(),
          },
        },
      });
    }
  };

  const getTimes = () => {
    const result = [];

    if (!service || !date) {
      return result;
    }

    const start_moment = moment(company.start, 'hh:mm:ss');
    const start_minutes = start_moment.hours() * 60 + start_moment.minutes();

    const end_moment = moment(company.end, 'hh:mm:ss');
    const end_minutes = end_moment.hours() * 60 + end_moment.minutes();

    const now_moment = moment();
    const now_minutes = now_moment.hours() * 60 + now_moment.minutes();

    for (let i = start_minutes; i < end_minutes; i += service.duration) {
      let isDisabled = moment().startOf('day').diff(moment(date).startOf('day'), 'days') >= 1;
      for (let j in appointments) {
        const app_start_moment = moment(appointments[j].start);
        const app_start_minutes = app_start_moment.hours() * 60 + app_start_moment.minutes();

        const app_end_moment = moment(appointments[j].end);
        const app_end_minutes = app_end_moment.hours() * 60 + app_end_moment.minutes();

        if (
          (moment().diff(moment(date).startOf('day'), 'days') === 0 && app_start_minutes <= now_minutes) ||
          (app_start_minutes >= i && app_start_minutes < i + service.duration) ||
          (app_end_minutes > i && app_end_minutes <= i + service.duration)
        ) {
          isDisabled = true;
          break;
        }
      }
      result.push(
        <TouchableOpacity
          disabled={isDisabled}
          key={i}
          onPress={() => {
            setSelect(null);
            setTime(i);
          }}
          cls={`row jc-between br round mb-3 p-3 ${time === i && 'bg-primary'}`}
        >
          <Text cls={`bold left ${isDisabled ? 't-light' : time === i && 't-white'}`}>
            {parseInt(i / 60) < 10 && '0'}
            {parseInt(i / 60)}:{i % 60 < 10 && '0'}
            {i % 60}
          </Text>
          {i === time && <Icon name="check" color="white" />}
        </TouchableOpacity>,
      );
    }
    return result;
  };

  return (
    <View cls="whole">
      <CoverView
        file={company.cover ? {uri: company.cover.path} : require('../../images/point-cover.png')}
        title={<Text cls="bolder f-5 t-white">{isTime ? 'Очередь по записи' : 'Живая очередь'}</Text>}
        isMessage
        bottomComponent={
          <View cls="whole jc-around">
            <View cls="ai-center">
              <Image cls="circle-4" source={{uri: company.logo && company.logo.path}} />
              <Text cls="f-5 bolder my-3 center">{company.name}</Text>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => setSelect('service')}
                cls="row shadow round ai-center px-3 py-4 mx-4 mb-3"
              >
                <Text cls="whole bolder">{service ? service.name : 'Выбрать категорию'}</Text>
                <Icon name="chevron-down" />
              </TouchableOpacity>
              {service && (
                <TouchableOpacity
                  onPress={() => setSelect('master')}
                  cls="row shadow round ai-center px-3 py-4 mx-4 mb-3"
                >
                  <Text cls="whole bolder">{master ? master.user.name : 'Выбрать сотрудника'}</Text>
                  <Icon name="chevron-down" />
                </TouchableOpacity>
              )}
              {isTime && (
                <TouchableOpacity
                  onPress={() => setSelect('date')}
                  cls="row shadow ai-center round px-3 py-4 mx-4 mb-3"
                >
                  <Text cls="whole bolder">{date ? date.format('DD MMMM') : 'Выбрать дату'}</Text>
                  <Icon name="chevron-down" />
                </TouchableOpacity>
              )}
              {date && service && (
                <TouchableOpacity
                  onPress={() => setSelect('time')}
                  cls="row shadow ai-center round px-3 py-4 mx-4 mb-3"
                >
                  <Text cls="whole bolder">
                    {time
                      ? `${parseInt(time / 60) < 10 ? '0' : ''}${parseInt(time / 60)}:${time % 60 < 10 ? '0' : ''}${
                          time % 60
                        }`
                      : 'Выбрать время'}
                  </Text>
                  <Icon name="chevron-down" />
                </TouchableOpacity>
              )}
            </View>
            <View cls="ai-center">
              <Button title={saveResp.loading ? 'Отправка ...' : 'Вставть в очередь'} size={4} onPress={onSave} />
            </View>
          </View>
        }
      />
      <RNModal isVisible={select === 'date'} useNativeDriver={true} onBackdropPress={() => setSelect(null)}>
        <View
          cls="whole bg-white p-4"
          style={{marginTop: 200, marginBottom: -30, borderTopLeftRadius: 15, borderTopRightRadius: 15}}
        >
          <Text cls="bold py-3">Выбрать дату</Text>
          <View cls="p-4">
            <CalendarPicker
              previousComponent={<Icon name="chevron-left" size={20} />}
              nextComponent={<Icon name="chevron-right" size={20} />}
              months={moment.months()}
              weekdays={moment.weekdaysMin(true)}
              onDateChange={d => {
                setSelect(null);
                setDate(d);
              }}
              selectedStartDate={date}
              startFromMonday={true}
              selectedDayColor={primary}
              selectedDayTextColor="white"
              width={WIDTH - 40}
              style={{width: '100%'}}
            />
          </View>
        </View>
      </RNModal>
      <RNModal isVisible={select === 'time'} onBackdropPress={() => setSelect(null)}>
        <View
          cls="whole bg-white p-4"
          style={{marginTop: 200, marginBottom: -20, borderTopLeftRadius: 15, borderTopRightRadius: 15}}
        >
          <Text cls="bold py-3">Выбрать время</Text>
          <ScrollView>
            <View cls="py-4">{getTimes()}</View>
          </ScrollView>
        </View>
      </RNModal>
      <RNModal isVisible={select === 'service'} useNativeDriver={true} onBackdropPress={() => setSelect(null)}>
        <View
          cls="whole bg-white p-4"
          style={{marginTop: 200, marginBottom: -20, borderTopLeftRadius: 15, borderTopRightRadius: 15}}
        >
          <Text cls="bold py-3">Выбрать категорию</Text>
          <ScrollView>
            <View cls="py-4">
              {services.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() => {
                    setSelect(null);
                    setService(item);
                  }}
                  cls={`row jc-between br round mb-3 px-3 py-4 ${service && service.id === item.id && 'bg-primary'}`}
                >
                  <Text cls={`bold left ${service && service.id === item.id && 't-white'}`}>{item.name}</Text>
                  {service && item.id === service.id && <Icon name="check" color="white" />}
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </View>
      </RNModal>
      <RNModal isVisible={select === 'master'} useNativeDriver={true} onBackdropPress={() => setSelect(null)}>
        <View
          cls="whole bg-white p-4"
          style={{marginTop: 200, marginBottom: -20, borderTopLeftRadius: 15, borderTopRightRadius: 15}}
        >
          <Text cls="bold py-3">Выбрать сотрудника</Text>
          <ScrollView>
            <View cls="py-4">
              <TouchableOpacity
                onPress={() => {
                  setSelect(null);
                  setMaster(null);
                }}
                cls={`row jc-between br round mb-3 px-3 py-4 ${!master && 'bg-primary'}`}
              >
                <Text cls={`bold left ${!master && 't-white'}`}>Выбрать сотрудника</Text>
                {!master && <Icon name="check" color="white" />}
              </TouchableOpacity>
              {positions.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() => {
                    setSelect(null);
                    setMaster(item.master);
                  }}
                  cls={`row jc-between br round mb-3 px-3 py-4 ${
                    master && master.id === item.master.id && 'bg-primary'
                  }`}
                >
                  <Text cls={`bold left ${master && master.id === item.master.id && 't-white'}`}>
                    {item.master.user.name}
                  </Text>
                  {master && item.master.id === master.id && <Icon name="check" color="white" />}
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </View>
      </RNModal>
    </View>
  );
};

export default AppointmentSave;
