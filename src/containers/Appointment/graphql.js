import {gql} from '@apollo/client';

export const FRAGMENT_APPOINTMENT_LIST = gql`
  fragment AppointmentList on AppointmentType {
    id
    start
    number
    status
    commentClient
    commentCompany
    score
    createdAt
    service {
      id
      name
      group {
        id
        company {
          id
          name
          logo {
            id
            path
          }
        }
      }
    }
    master {
      id
      specialty
      user {
        id
        name
        avatar {
          id
          path
        }
      }
    }
  }
`;

export const QUERY_APPOINTMENT_LIST = gql`
  query AppointmentList($companyId: ID, $clientId: ID, $start: DateTime, $end: DateTime) {
    appointmentList(service_Group_Company: $companyId, start_Gte: $start, start_Lte: $end, client: $clientId) {
      ...AppointmentList
    }
  }
  ${FRAGMENT_APPOINTMENT_LIST}
`;

export const QUERY_APPOINTMENT_DETAIL = gql`
  query AppointmentDetail($id: ID!) {
    appointmentDetail(id: $id) {
      ...AppointmentList
    }
  }
  ${FRAGMENT_APPOINTMENT_LIST}
`;

export const MUTATION_APPOINTMENT_SAVE = gql`
  mutation AppointmentSave($input: AppointmentSaveInput!) {
    appointmentSave(input: $input) {
      obj {
        ...AppointmentList
      }
      errors {
        field
        messages
      }
    }
  }
  ${FRAGMENT_APPOINTMENT_LIST}
`;

export const MUTATION_APPOINTMENT_SCORE = gql`
  mutation AppointmentScore($input: AppointmentScoreInput!) {
    appointmentScore(input: $input) {
      obj {
        id
        commentClient
        score
      }
      errors {
        field
        messages
      }
    }
  }
`;

export const MUTATION_APPOINTMENT_DELETE = gql`
  mutation AppointmentDelete($input: AppointmentDeleteInput!) {
    appointmentDelete(input: $input) {
      obj {
        id
      }
    }
  }
`;
