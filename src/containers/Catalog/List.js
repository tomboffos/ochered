import React from 'react';
import {Dimensions, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {View, Text, Image, TouchableOpacity} from '../../styles';
import {useQuery} from '@apollo/client';
import {QUERY_CATALOG_LIST} from './graphql';
import Query from '../../components/Query';
import Header from '../../components/Header';

const WIDTH = Dimensions.get('window').width * 0.4;

const CatalogList = () => {
  const navigation = useNavigation();

  const query = useQuery(QUERY_CATALOG_LIST);

  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="bolder f-5">Встать в очередь</Text>} />
      {Query(query) || (
        <View cls="whole">
          <ScrollView>
            <View cls="row jc-around px-2 py-4" style={{flexWrap: 'wrap'}}>
              {query.data.catalogList.map(item => (
                <TouchableOpacity
                  onPress={() => navigation.push('CatalogDetail', {catalog: item})}
                  key={item.id}
                  cls="round shadow my-3 jc-center ai-center"
                  style={{width: WIDTH, height: WIDTH}}
                >
                  <Image
                    style={{width: 50, height: 50, resizeMode: 'contain'}}
                    source={require('../../images/catalog.png')}
                  />
                  <Text cls="mt-3 bold">{item.name}</Text>
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default CatalogList;
