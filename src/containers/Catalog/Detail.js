import React from 'react';
import {View, Text} from '../../styles';
import CategoryList from '../Category/List';
import Header from '../../components/Header';

const CatalogDetail = ({route}) => {
  const {catalog} = route.params;

  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="f-5 bolder">{catalog.name}</Text>} />
      <View cls="whole py-2">
        <CategoryList catalog={catalog} />
      </View>
    </View>
  );
};

export default CatalogDetail;
