import {gql} from '@apollo/client';

export const QUERY_CATALOG_LIST = gql`
  query CatalogList {
    catalogList {
      id
      name
      image {
        id
        path
      }
    }
  }
`;
