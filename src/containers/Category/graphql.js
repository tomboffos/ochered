import {gql} from '@apollo/client';

export const QUERY_CATEGORY_LIST = gql`
  query CatalogList($catalogId: ID!) {
    categoryList(catalog: $catalogId) {
      id
      name
      companiesCount
      cover {
        id
        path
      }
    }
  }
`;
