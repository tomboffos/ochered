import React from 'react';
import {View, Text, TextInput} from '../../styles';
import Icon from '../../components/Icon';
import CompanyList from '../Company/List';
import CoverView from '../../components/CoverView';

const CategoryDetail = ({route}) => {
  const {category} = route.params;

  return (
    <View cls="whole">
      <CoverView
        title={<Text cls="t-white bolder f-5">{category.name}</Text>}
        file={category.cover ? {uri: category.cover.path} : require('../../images/category.png')}
        bottomComponent={<CompanyList category={category} />}
      />
    </View>
  );
};

export default CategoryDetail;
