import React from 'react';
import {FlatList} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {View, Image, Text, TouchableOpacity, TextInput} from '../../styles';
import Icon from '../../components/Icon';
import {useQuery} from '@apollo/client';
import {QUERY_CATEGORY_LIST} from './graphql';
import Query from '../../components/Query';

const CategoryList = ({catalog}) => {
  const navigation = useNavigation();

  const categoryList = useQuery(QUERY_CATEGORY_LIST, {variables: {catalogId: catalog.id}});

  return (
    <View cls="whole">
      {Query(categoryList) || (
        <FlatList
          data={categoryList.data.categoryList}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => navigation.push('CategoryDetail', {category: item})}
              cls="row shadow round mx-3 mb-3 p-4"
            >
              <Image
                style={{width: 30, height: 30, resizeMode: 'contain'}}
                source={require('../../images/catalog.png')}
              />
              <Text cls="whole bolder ml-3">{item.name}</Text>
              {/*<Text cls="mr-3">({item.companiesCount})</Text>*/}
              <Icon name="chevron-right" />
            </TouchableOpacity>
          )}
          contentContainerStyle={{paddingVertical: 20}}
        />
      )}
    </View>
  );
};

export default CategoryList;
