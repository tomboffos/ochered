import React from 'react';
import {View, Text} from '../../styles';
import Header from '../../components/Header';
import Button from '../../components/Button';

const ChatList = () => {
  return (
    <View cls="whole">
      <Header centerComponent={<Text cls="bolder f-5">Чат</Text>} />
      <View cls="whole jc-center ai-center">
        <Text cls="f-4 bolder center mb-4">У вас пока нет сообщений</Text>
        <Button title="Начать переписку" />
      </View>
    </View>
  );
};

export default ChatList;
