import {gql} from '@apollo/client';

export const QUERY_POSITION_LIST = gql`
  query PositionList($serviceId: ID) {
    positionList(service: $serviceId) {
      id
      master {
        id
        specialty
        user {
          id
          name
        }
      }
    }
  }
`;
