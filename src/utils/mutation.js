export const updateCreate = (
  {query, variables, getTarget = (target, item) => [item, ...target]},
  callback = created => {},
) => {
  return (cache, response) => {
    const mutate = response.data ? response.data[Object.keys(response.data)[0]] : null;
    if (mutate.obj) {
      if (query) {
        try {
          const readQuery = cache.readQuery({query, variables});
          const targetName = Object.keys(readQuery)[0];
          cache.writeQuery({query, variables, data: {[targetName]: getTarget(readQuery[targetName], mutate.obj)}});
        } catch (e) {}
      }
      if (callback) callback(mutate.obj);
    }
  };
};

export const updateDelete = (
  {query, variables, getTarget = (target, deletedId) => target.filter(x => x.id !== deletedId)},
  callback,
) => {
  return (cache, response) => {
    const mutate = response.data ? response.data[Object.keys(response.data)[0]] : null;
    if (mutate.deletedId || mutate.obj) {
      const id = mutate.deletedId || mutate.obj.id;
      try {
        const readQuery = cache.readQuery({query, variables});
        const targetName = Object.keys(readQuery)[0];
        cache.writeQuery({
          query,
          variables,
          data: {[targetName]: getTarget(readQuery[targetName], id)},
        });
      } catch (e) {}
      if (callback) callback();
    }
  };
};
