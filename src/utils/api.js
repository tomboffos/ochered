import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export const SERVER_URL = 'http://localhost:8000';
// export const SERVER_URL = 'https://b-time.kz';

export const request = async ({method = 'get', url, data, headers}) => {
  const token = await AsyncStorage.getItem('token');
  const auth_header = token ? {Authorization: 'JWT ' + token} : {};
  return axios({
    method,
    url: `${SERVER_URL}${url}/`,
    data,
    headers: {'Content-Type': 'application/json', ...headers, ...auth_header},
  });
};
