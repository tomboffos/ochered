Number.prototype.toCurrency = function () {
  return this.valueOf()
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
