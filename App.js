import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {useQuery, useApolloClient} from '@apollo/client';
import {QUERY_USER} from './src/containers/Client/graphql';
import {View, Text} from './src/styles';
import {AuthNavigation, MainNavigation} from './Navigation';

const App = () => {
  const client = useApolloClient();

  const query = useQuery(QUERY_USER, {
    onCompleted: data => {
      if (!data || data.user === null || data.user.role !== 'CLIENT') {
        AsyncStorage.removeItem('token');
      }
    },
  });
  const user = query.data ? query.data.user : null;

  const onLogin = newToken => {
    AsyncStorage.setItem('token', newToken);
    query.refetch();
  };

  const onLogout = () => {
    AsyncStorage.removeItem('token');
    client.cache.writeQuery({query: QUERY_USER, data: {user: null}});
  };

  if (!query.called || (!query.data && query.loading)) {
    return (
      <View cls="whole ai-center jc-center">
        <Text cls="text-primary">Загрузка...</Text>
      </View>
    );
  }

  if (!query.data || !user || (user && user.role !== 'CLIENT')) {
    return <AuthNavigation onLogin={onLogin} user={user} userLoading={query.loading} />;
  }

  return <MainNavigation onLogout={onLogout} />;
};

export default App;
