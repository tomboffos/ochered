import React from 'react';
import {AppRegistry} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {setContext} from 'apollo-link-context';
import {ApolloProvider, ApolloClient, HttpLink, InMemoryCache} from '@apollo/client';
import 'moment/locale/ru';
import App from './App';
import {name as appName} from './app.json';
import {SERVER_URL} from './src/utils/api';
import './settings';

const authLink = setContext(async (_, {headers}) => {
  const token = await AsyncStorage.getItem('token');

  return {
    headers: {
      ...headers,
      Authorization: token ? `JWT ${token}` : '',
    },
  };
});

const httpLink = new HttpLink({
  uri: `${SERVER_URL}/graphql/`,
});

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'all',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    errorPolicy: 'all',
  },
};

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink),
  defaultOptions,
});

const Core = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

AppRegistry.registerComponent(appName, () => Core);
